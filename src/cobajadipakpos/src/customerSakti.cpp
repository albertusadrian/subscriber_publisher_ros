#include "ros/ros.h"
#include <cobajadipakpos/kurir.h>

void kurirCallback(const cobajadipakpos::kurir& msg)
{
  ROS_INFO("\nData diterima:\n Perangko: %f\nSurat: %f\nBelanja: %f\n", msg.perangko, msg.surat, msg.belanja);
}

int main(int argc, char **argv)
{
  
  ros::init(argc, argv, "customerSakti");
  ros::NodeHandle n;
  ros::Subscriber sub = n.subscribe("/kurir_sakti", 10, kurirCallback);
  ros::spin();
  return 0;
}
