# Contoh Publisher dan Subcriber
Berikut adalah tutorial membuat subcriber dan publisher

Tutorial singkat:
```bash
# Buka terminal dan workspace anda
cd ~/yourworkspace/
mkdir src/
cd /src
catkin_init_workspace
cd ..
catkin_make

# Setelah itu di direktory yang sama lakukan clone pada directory src
cd src/
git clone https://gitlab.com/ynugraha/subriber_publisher_ros.git
cd ..
catkin_make
```

Pasangan node:
1. pub: pakpos --> sub: customer
2. pub: pakposSakti --> sub: customerSakti

